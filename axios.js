const axios = require("axios");

const getDataAsyncAwait = async () => {
  try {
    const result = await axios(
      "https://indonesia-public-static-api.vercel.app/api/heroes"
    );
    result.data.forEach((value) => console.log(value.name));
  } catch (error) {
    console.log(error);
  }
};

const getDataCallback = () => {
  axios("https://indonesia-public-static-api.vercel.app/api/heroes")
    .then((result) => {
      result.data.forEach((value) => console.log(value.name));
    })
    .catch((error) => console.log(error));
};

getDataAsyncAwait();
// getDataCallback();
