let stok = 5;

const buatKopi = () =>
  new Promise((resolve, reject) => {
    if (stok - 1 <= 0) {
      reject("Stok habis!");
    }
    stok--;
    resolve(stok);
  });

for (let index = 0; index < 6; index++) {
  buatKopi()
    .then((result) => {
      console.log(`Kopi dibuat, sisa stok : ${result}`);
    })
    .catch((error) => {
      console.log(error);
    });
}
