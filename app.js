const fs = require("fs");

const option = {
  encoding: "utf-8",
};


const callback = (err, data) => {
  console.log("muncul pertama");
  if (err) {
    return console.error("Error: " + err);
  }
  console.log("Isi file: " + data);
};

const bacaFile = () => {
  return new Promise((resolve, reject) => {
    fs.readFile("./contoh.txt", option, callback);
    setTimeout(() => {
      resolve("muncul kedua");
    }, 1000);
  });
};

bacaFile().then((result) => console.log(result));

// fs.readFile("./contoh.txt", option, callback);

// console.log("kedua");
